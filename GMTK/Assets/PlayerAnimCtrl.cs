using System.Timers;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimCtrl : MonoBehaviour
{
    public static PlayerAnimCtrl instance;
    SpriteRenderer sr;
    [SerializeField]Sprite idel;
    [SerializeField]Sprite left;
    [SerializeField]Sprite right;
    [SerializeField]Sprite jump;
    [SerializeField]Sprite die;
    private void Awake() {
        if(instance==null){
            instance=this;
        }
        else{
            Destroy(gameObject);
        }
    }
     private void Start() {
        sr = GetComponent<SpriteRenderer>();
     }
    void Update()
    {
        SetHorAnimState();
    }

    private void SetHorAnimState()
    {
        float dir;
        dir = Input.GetAxisRaw("Horizontal");

        if(dir==0){
            sr.sprite = idel;
        }
        if(dir>0){
            sr.sprite = right;
        }
             if(dir<0){
            sr.sprite = left;
        }
    }
    public void SetPlayerJump(){
        sr.sprite=jump;
    }
    public void SetPlayerDeath(){
        sr.sprite = die;
    }

    
}
