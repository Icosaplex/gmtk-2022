using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCTRL : MonoBehaviour
{
    // Access player controller
    public static PlayerCTRL instance;
    private void Awake() {
    if(instance==null){
        instance = this;
    }
    else{
        Destroy(gameObject);
    }
    }
    Feet feet;
    void Start()
    {
        feet = FindObjectOfType<Feet>();
    }

    // TODO Connect player position to camera
    public float playerYPosition;
    bool playerIsAlive = true;
    void Update()
    {
        if (!playerIsAlive) {
          // TODO death animations
          // DEBUG Death!!!
          Debug.Log("player death animation!!!");
          GameCtrl.instance.GameOver();
        }

        // player position
        playerYPosition = this.gameObject.transform.position.y;
    }


    // Player position broadcast
    public float GetPlayerYPosition(){
        return playerYPosition;
    }

  private void OnTriggerEnter2D(Collider2D other) {
  if(other.gameObject.CompareTag("ground")){
      AkSoundEngine.PostEvent("Player_Land", this.gameObject);
      AnimCtrl.instance.PlayPoofVfx(feet.gameObject.transform.position);
    }


  // Player dies
  if(other.gameObject.CompareTag("Death")){
      playerIsAlive=false;
    }
  }
}
