using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    [SerializeField] float cameraMoveSpeed = 10f;
    bool pauseMovement = false;
    // float cameraTrackPositionOffset = 7f;
    // float interpolationRatio = 0.001f;
    float playerYPosition;
    // Update is called once per frame
    void Update()
    {
        // Vector3 CameraPosition = Camera.main.ScreenToWorldPoint(new Vector3(0,0,0));
        // // Debug.Log(CameraPosition.y);
        // playerYPosition = PlayerCTRL.instance.GetPlayerYPosition();
        // if (playerYPosition >= (CameraPosition.y + cameraTrackPositionOffset)) {
        //     Vector3 vectorPlayerYPosition = new Vector3(0, playerYPosition, 0);
        //     Camera.main.transform.Translate(Vector3.Lerp(vectorPlayerYPosition, (CameraPosition), interpolationRatio));
        // }
        // else
        // {
        // }

        pauseMovement = pauseMenu.instance.GetPauseStatus();
        if (!pauseMovement)
        {
            Camera.main.transform.Translate(0, cameraMoveSpeed * Time.deltaTime, 0);
        }
        else
        {
            Camera.main.transform.Translate(0, 0, 0);
        }

    }
}
