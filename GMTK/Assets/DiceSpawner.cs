using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceSpawner : MonoBehaviour
{
    [SerializeField] GameObject die;
    [SerializeField]float spawnTime;

  private void Start() 
  {
    StartCoroutine("StartSpawningDice");
  }

  IEnumerator StartSpawningDice(){

    yield return new WaitForSeconds(spawnTime);
    SpawnDice();
    StartCoroutine("StartSpawningDice");
  }

    private void SpawnDice()
    {
        float shift = UnityEngine.Random.Range(-5,5);
        Vector3 shiftedPosition = new Vector3(transform.position.x + shift, transform.position.y, transform.position.z);
        Instantiate(die, shiftedPosition, Quaternion.identity);
    }

}
