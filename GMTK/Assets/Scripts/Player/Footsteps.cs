using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Footsteps : MonoBehaviour
{
    bool isGrounded;
    float timer = 0.0f;
    [SerializeField] float footstepSpeed = 0.15f;

    [SerializeField] Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isGrounded && System.Math.Abs(rb.velocity.x) > 0.1f) {
            if (timer > footstepSpeed) {
                AkSoundEngine.PostEvent("Footsteps", this.gameObject);
                timer = 0.0f;
            }
        }

        timer += Time.deltaTime;
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.CompareTag("ground")){
            isGrounded = true;
        }
    }
    private void OnTriggerExit2D(Collider2D other) {
        if(other.gameObject.CompareTag("ground")){
            isGrounded = false;
            // ALERT This approach doesn't feel so great. Not responsive enough
            // TODO polish this method. Make more responsive
        }
    }
}
