using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimCtrl : MonoBehaviour
{
public static AnimCtrl instance;
[SerializeField]GameObject poofVFX;
[SerializeField]GameObject sparklesVFX;


private void Awake() 
{
    if(instance==null){
        instance=this;
    }
    else{
        Destroy(gameObject);
    }
}
public void PlayPoofVfx(Vector2 pos)
{
Instantiate(poofVFX, pos, Quaternion.identity);
}

public void PlaySparklesVfx(Vector2 pos)
{
Instantiate(sparklesVFX, pos, Quaternion.identity);
}

}
