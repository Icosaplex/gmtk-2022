using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreScreen : MonoBehaviour
{
    [SerializeField] private TMP_Text scoreText;

    [SerializeField]float scoreMulti;

    float score;

    public const string HighScoreKey = "HighScore";

    // Update is called once per frame
    void Update()
    {
        score = GameCtrl.instance.score;
        scoreText.text=($"HighScore: "+PlayerPrefs.GetInt(HighScoreKey, 0).ToString());
    }

     private void OnDestroy() {
       int currentHighScore = PlayerPrefs.GetInt(HighScoreKey, 0);

       if(score>currentHighScore){
        PlayerPrefs.SetInt(HighScoreKey,Mathf.FloorToInt(score));
       }
    }
}
