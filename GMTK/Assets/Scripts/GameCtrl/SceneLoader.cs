using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
 public void LoadStartGame()
 {
SceneManager.LoadScene("Darron");
 }


 public void LoadMainMenuGame()
 {
SceneManager.LoadScene("MainMenu");
 }


 public void LoadSceneName(string scene)
 {
SceneManager.LoadScene(scene);
 }

 public void ExitGame(){
    Application.Quit();
 }
}
