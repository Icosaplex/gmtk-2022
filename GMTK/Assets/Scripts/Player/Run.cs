using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Run : MonoBehaviour
{
    bool playerIsAlive = true;
    public int speedBuff;
    [SerializeField]float moveSpeed=5f;
    Rigidbody2D rb;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        speedBuff= 3;
    }

   
    void Update()
    {
       Move();
    }

    private void Move()
    {
        float dir = Input.GetAxisRaw("Horizontal");
        if (playerIsAlive){
            rb.velocity = new Vector2(dir*moveSpeed * speedBuff,rb.velocity.y);
        }
        else
        {
            rb.velocity = new Vector2(0,rb.velocity.y);
        }

    }

    public void SetSpeedBuff(int buff){
        speedBuff=buff;
    }

    private void OnTriggerEnter2D(Collider2D other) {
        // Player dies
        if(other.gameObject.CompareTag("Death")){
            playerIsAlive=false;
        }
    }

}
