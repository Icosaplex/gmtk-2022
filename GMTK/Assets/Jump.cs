using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour
{
    // ALERT control downward ark with Gravity Scale :: Rigid Body 2D

    void Start()
    {
    }

    [SerializeField] BoxCollider2D boxCollider2D;
    bool playerIsAlive = true;
    
    // Has the Wwise death event been triggered? 
    bool deathTriggered = false;
    float diceValue;
    // Variable Jump controls
    [SerializeField] Rigidbody2D rb;
    [SerializeField] float buttonTime = 0.5f;
    [SerializeField] float jumpHeight = 1.5f;
    float cancelJumpMomentum = 50;
    [SerializeField] float forceMultiplier = 2.1f;
    float jumpTime;
    bool jumping;
    bool jumpCancelled;

    // Jump check
    bool isGrounded;
    private void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.CompareTag("ground")){
            isGrounded = true;
        }
        // Player dies
        if(other.gameObject.CompareTag("Death")){
            playerIsAlive=false;
        }
    }
    private void OnTriggerExit2D(Collider2D other) {
        if(other.gameObject.CompareTag("ground")){
            isGrounded = false;
            // ALERT This approach doesn't feel so great. Not responsive enough
            // TODO polish this method. Make more responsive
        }
    }


    // Update is called once per frame
    private void Update()
    {
        // get dice pickup
        diceValue = GameCtrl.instance.GetDice();
        // jump dice multiplier
        float diceForce = diceValue * forceMultiplier;
        // Variable Jump
        if (playerIsAlive)
        {
            if (Input.GetKeyDown(KeyCode.Space) && isGrounded == true)
            {
                float jumpForce = Mathf.Sqrt(jumpHeight * -2 * (Physics2D.gravity.y * rb.gravityScale));
                rb.AddForce(new Vector2(0, jumpForce * diceForce), ForceMode2D.Impulse);
                jumping = true;
                jumpCancelled = false;
                jumpTime = 0;
                AkSoundEngine.PostEvent("Player_Jump", this.gameObject);
            }
            if (jumping)
            {
                jumpTime += Time.deltaTime;
                if (Input.GetKeyUp(KeyCode.Space))
                {
                    jumpCancelled = true;
                }
                if (jumpTime > buttonTime)
                {
                    jumping = false;
                }
            }
        }
        else
        {
            // PLAYER DIES
            if (!deathTriggered) {
                AkSoundEngine.PostEvent("Death", this.gameObject);
                deathTriggered = true;
            }
            StartCoroutine("DeathJump");
        }

        if(!isGrounded){
            PlayerAnimCtrl.instance.SetPlayerJump();
        }
    }
    private void FixedUpdate()
    {
        if(jumpCancelled && jumping && rb.velocity.y > 0)
        {
            rb.AddForce(Vector2.down * cancelJumpMomentum);
        }
    }
    IEnumerator DeathJump(){
        rb.AddForce(new Vector2(0, 15));
        yield return new WaitForSeconds(0.1f);
        PlayerAnimCtrl.instance.SetPlayerDeath();
        boxCollider2D.enabled = false;
        rb.AddForce(new Vector2(0, 1));
        yield return new WaitForSeconds(0.5f);
        rb.AddForce(new Vector2(0, -20));
        yield return new WaitForSeconds(1);
    }
}