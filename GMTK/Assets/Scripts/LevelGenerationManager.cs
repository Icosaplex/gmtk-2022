using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerationManager : MonoBehaviour
{
    [SerializeField] GameObject cameraObject;
    [SerializeField] GameObject terrainSampleBlock;
    [SerializeField] GameObject templateTerrainOutputBlock;

    [SerializeField] float terrainBlockSize = 40.0f;
    float yLastBlock = -5.0f;

    // Start is called before the first frame update
    void Start()
    {
        // Generate the first block of terrain
        generateNextBlock();
    }

    // Update is called once per frame
    void Update()
    {
        if (cameraObject.transform.position.y > yLastBlock) {
            generateNextBlock();
        }
    }

    void generateNextBlock()
    {
        // FIXME: Get rid of literal dimensions in instantiation
        GameObject nextBlock = Instantiate(templateTerrainOutputBlock, new Vector3(-20, yLastBlock + ( 2 * terrainBlockSize), 0), Quaternion.identity);

        OverlapWFC overlap = nextBlock.GetComponent<OverlapWFC>();
        overlap.Generate();
        yLastBlock += 2 * terrainBlockSize;
    }
}
