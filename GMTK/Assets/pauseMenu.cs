using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pauseMenu : MonoBehaviour
{
    public static pauseMenu instance;
    private void Awake() {
        if(instance==null){
            instance = this;
        }
        else{
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        PausePanel.SetActive(false);
    }

    bool gameIsPaused = false;
    [SerializeField] GameObject PausePanel;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)){
            PauseGame();
         }
    }
    void PauseGame ()
    {
        AkSoundEngine.PostEvent("Pause", PausePanel);
        PausePanel.SetActive(true);
        Time.timeScale = 0;
        gameIsPaused = true;
    }
    public void ResumeGame ()
    {
        AkSoundEngine.PostEvent("Resume", PausePanel);
        PausePanel.SetActive(false);
        Debug.Log("Continue");
        gameIsPaused = false;
        Time.timeScale = 1;
    }
    public bool GetPauseStatus(){
        return gameIsPaused;
    }


}
