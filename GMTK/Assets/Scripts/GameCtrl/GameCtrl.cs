using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class GameCtrl : MonoBehaviour
{
    public static GameCtrl instance;
    [SerializeField]TMP_Text timerText;
    [SerializeField]TMP_Text diceText;
    [SerializeField]TMP_Text scoreText;
  [SerializeField]TMP_Text gameOverScoreText;
    [SerializeField]GameObject gameOverScreen;
    [SerializeField]float deathScreenDelay;

    int dice = 3;
    public int score;
    float time;
    Run playerRun;

    public int timer;

    private void Awake() {
        if(instance==null){
            instance = this;
        }    
        else{
            Destroy(gameObject);
        }
    }


    private void Start() {
        score = 0;
        playerRun = FindObjectOfType<Run>();
        scoreText.text = $"Score: {score}"; 
    }

    private void Update() {
        UpdateTime();
    }

    private void UpdateTime()
    {
        time = Time.time;
        timer = Mathf.RoundToInt(time);
        timerText.text=timer.ToString();
    }

    public void SetDice(int diceRolled){
        dice=diceRolled;
        score += dice;
        // playerRun.SetSpeedBuff(dice);
        UpdateDisplay();
    }

    private void UpdateDisplay()
    {
        diceText.text = $"Rolled: {dice}";
        scoreText.text = $"Score: {score}";
    }

    public int GetDice(){
        return dice;
    }

    // DEATH!!!!
    bool isAlive = true;
    // TODO global death
    // Debug.Log(isAlive);
    public void SetDeathState(bool isAlive){
        // return isAlive;
    }
    public bool GetDeathState(){
        return isAlive;
    }


    public void GameOver(){

           Invoke("PlayerDie",deathScreenDelay);
    }

    private void PlayerDie()
    {
               gameOverScreen.SetActive(true);
           gameOverScoreText.text = $"Score: {score}"; 
    }
}
