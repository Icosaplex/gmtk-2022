using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomDieD6 : MonoBehaviour
{

[SerializeField] int diceNo;
 [SerializeField]Sprite[] diceImg;
 SpriteRenderer sr;
 

  private void Start()
   {
    sr=GetComponent<SpriteRenderer>();
    float ranNo =Random.Range(1,7);
    diceNo=Mathf.RoundToInt(ranNo);
       sr.sprite =  diceImg[diceNo-1];
}
private void OnTriggerEnter2D(Collider2D other) {
    if(other.gameObject.CompareTag("player")){
        GameCtrl.instance.SetDice(diceNo);
        AkSoundEngine.PostEvent("Item_Dice_Pickup", other.gameObject);
        AnimCtrl.instance.PlaySparklesVfx(other.gameObject.transform.position);
        Destroy(gameObject);
    }
  
}

public void RollDice()
{
    float ranNo =Random.Range(1,7);
    diceNo=Mathf.RoundToInt(ranNo);
   sr.sprite =  diceImg[diceNo-1];

}



}
