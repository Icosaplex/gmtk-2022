using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dice : MonoBehaviour
{
public int diceNo;

private void OnTriggerEnter2D(Collider2D other) {
    if(other.gameObject.CompareTag("player")){
        GameCtrl.instance.SetDice(diceNo);
        AkSoundEngine.PostEvent("Item_Dice_Pickup", other.gameObject);
        AnimCtrl.instance.PlaySparklesVfx(other.gameObject.transform.position);
        Destroy(gameObject);
    }

 
}

}
